module.exports = {
  title: 'Obsidian schema v1',
  type: 'object',
  required: ['passes', 'failures', 'success', 'startTimeUtc', 'endTimeUtc'],
  properties: {
    executions: {
      type: 'array',
      items: {
        type: 'object',
        required: ['testName'],
        properties: {
          testName: { type: 'string' },
          duration: { type: 'number' },
          classname: { type: 'string' },
          testFile: {type: 'string' },
          message: {type: 'string' },
          type: {type: 'string' },
          stackTrace: {type: 'string' },
          succeeded: {type: 'boolean' },
        },
      },
    },
    startTimeUtc: {
      type: 'string',
    },
    endTimeUtc: {
      type: 'string',
    },
    passes: {
      type: 'number',
    },
    failures: {
      type: 'number',
    },
    success: {
      type: 'boolean',
    },
  },
};
