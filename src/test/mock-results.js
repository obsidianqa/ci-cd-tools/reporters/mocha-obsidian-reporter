module.exports = function(stats, options) {
  const data =
    {
      executions: [
        {
          testName: 'Obsidian Test Mapper Service executeMapper',
          duration: 0.000002,
          classname: 'Mapper Service executeMapper',
          testFile: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/mapperService__test.js',
          message: '',
          type: '',
          stackTrace: '',
          succeeded: true,
        },
        {
          testName: 'Another suite! Test Runs handleCommand(commandContext) pass',
          duration: 0.000001,
          classname: 'Test Runs handleCommand(commandContext) pass',
          testFile: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/testRuns__test.js',
          message: '',
          type: '',
          stackTrace: '',
          succeeded: true,
        },
        {
          testName: 'Another suite! Test Runs handleCommand(commandContext) ' +
          'If help command is used, return test run help info',
          duration: 0.000003,
          classname: 'Test Runs handleCommand(commandContext) fail',
          testFile: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/testRuns__test.js',
          message: 'expected true to be false',
          type: 'Error',
          stackTrace: 'AssertionError: expected true to be false\n    at Context.<anonymous> ' +
          '(test/testRuns__test.js:136:39)\n    at processTicksAndRejections (internal/process/task_queues.js:93:5)',
          succeeded: false,
        },
      ],
      passes: 2,
      failures: 1,
      success: false,
    };
  return data;
};
