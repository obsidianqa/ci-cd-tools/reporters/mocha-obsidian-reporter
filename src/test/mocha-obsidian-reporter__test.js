/* eslint-env mocha */

var Reporter = require('../index');
var Mocha = require('mocha');
var Runner = Mocha.Runner;
var Suite = Mocha.Suite;
var Test = Mocha.Test;

var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');

var chai = require('chai');
chai.use(require('chai-json'));
chai.use(require('chai-json-schema'));

var expect = chai.expect;
var FakeTimer = require('@sinonjs/fake-timers');
// var mockJson = require('./mock-results');
var obsidianSchema = require('./obsidian-schema');

// var testConsole = require('test-console');

var debug = require('debug')('mocha-obsidian-reporter:tests');

describe('mocha-obsidian-reporter', function() {
  var filePath;
  var MOCHA_FILE;
  var stdout;

  //   function mockStdout() {
  //     stdout = testConsole.stdout.inspect();
  //     return stdout;
  //   }

  function createTest(name, options, fn) {
    if (typeof options === 'function') {
      fn = options;
      options = null;
    }
    options = options || {};

    // null fn means no callback which mocha treats as pending test.
    // undefined fn means caller wants a default fn.
    if (fn === undefined) {
      fn = function() {};
    }

    var test = new Test(name, fn);
    test.file = options.file;

    var duration = options.duration;
    if (duration != null) {
      // mock duration so we have consistent output
      Object.defineProperty(test, 'duration', {
        set: function() {
          // do nothing
        },
        get: function() {
          return duration;
        },
      });
    }

    return test;
  }

  function runTests(reporter, options, callback) {
    if (!callback) {
      callback = options;
      options = null;
    }
    options = options || {};
    options.invalidChar = options.invalidChar || '';
    options.title = options.title || 'Obsidian Test';
    options.startTimeUtc = '2021-04-26T03:26:44.000Z';
    options.endTimeUtc = '2021-04-23T00:10:28.000Z';

    var runner = reporter._runner;
    var rootSuite = runner.suite;

    var suite1 = Suite.create(rootSuite, options.title);
    suite1.addTest(createTest('Mapper Service executeMapper', {
      duration: 0.002,
      file: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/mapperService__test.js',
    }));

    var suite2 = Suite.create(rootSuite, 'Another suite!');
    suite2.addTest(createTest('Test Runs handleCommand(commandContext) pass', {
      duration: 0.001,
      file: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/testRuns__test.js',
    }));

    suite2.addTest(createTest('Test Runs handleCommand(commandContext) fail',
      {
        duration: 0.003,
        file: '/Users/RyanMalmoe/Documents/Obsidian/obsidian-cli/cli/test/testRuns__test.js',
      }, function(done) {
        var err = new Error('expected true to be false');
        // eslint-disable-next-line max-len
        err.stack = 'AssertionError: expected true to be false\n    at Context.<anonymous> (test/testRuns__test.js:136:39)\n    at processTicksAndRejections (internal/process/task_queues.js:93:5)';
        done(err);
      }));

    var _onSuiteEnd = reporter.onSuiteEnd.bind(reporter);

    reporter._onSuiteEnd = function(suite) {
      if (suite === rootSuite) {
        // root suite took no time to execute
        reporter._Date.clock.tick(0);
      } else if (suite === suite1) {
        // suite1 took an arbitrary amount of time that includes time to run each test + setup and teardown
        reporter._Date.clock.tick(100001);
      } else if (suite === suite2) {
        reporter._Date.clock.tick(400005);
      }

      return _onSuiteEnd(suite);
    };

    runRunner(runner, callback);
  }

  //   function assertJsonEquals(actual, expected) {
  //     expect(actual).to.be.a.jsonFile().and.contain.jsonWithProps(expected);
  //   }
  function assertJsonSchema(actual, expected) {
    expect(actual).to.be.jsonSchema(expected);
  }

  function verifyMochaSchema(runner, path, options) {
    var now = (new Date()).toISOString();
    debug('verify', now);
    var output = fs.readFileSync(path, 'utf-8');
    assertJsonSchema(JSON.parse(output), obsidianSchema);
    debug('done', now);
  }

  //   function verifyMochaFile(runner, path, options) {
  //     var now = (new Date()).toISOString();
  //     debug('verify', now);
  //     var props = mockJson(runner.stats, options);

  //     assertJsonEquals(path, {testName: 'Obsidian Test Mapper Service executeMapper'});
  //     debug('done', now);
  //   }

  function removeTestPath(callback) {
    rimraf(__dirname + '/output', function(err) {
      if (err) {
        return callback(err);
      }

      // tests that exercise defaults will write to $CWD/test-results.json
      rimraf(__dirname + '/../test-results.json', callback);
    });
  }

  function createRunner() {
    // mocha always has a root suite
    var rootSuite = new Suite('', 'root', true);

    // We don't want Mocha to emit timeout errors.
    // If we want to simulate errors, we'll emit them ourselves.
    // rootSuite.enableTimeouts(false);
    rootSuite.timeout(0);

    return new Runner(rootSuite);
  }

  function createReporter(options) {
    options = options || {};
    filePath = path.join(path.dirname(__dirname), options.mochaFile || '');

    var mocha = new Mocha({
      reporter: Reporter,
      allowUncaught: true,
    });

    return new mocha._reporter(createRunner(), {
      reporterOptions: options,
      Date: FakeTimer.createClock(0).Date,
    });
  }

  function runRunner(runner, callback) {
    runner.run(function(failureCount) {
      // Ensure uncaught exception handlers are cleared before we execute test assertions.
      // Otherwise, this runner would intercept uncaught exceptions that were already handled by the mocha instance
      // running our tests.
      runner.dispose();

      callback(failureCount);
    });
  }

  //   function getFileNameWithHash(path) {
  //     var filenames = fs.readdirSync(path);
  //     var expected = /(^results\.)([a-f0-9]{32})(\.json)$/i;

  //     for (var i = 0; i < filenames.length; i++) {
  //       if (expected.test(filenames[i])) {
  //         return filenames[i];
  //       }
  //     }
  //   }

  before(function(done) {
    // cache this
    MOCHA_FILE = process.env.MOCHA_FILE;

    removeTestPath(done);
  });

  after(function() {
    // reset this
    process.env.MOCHA_FILE = MOCHA_FILE;
  });

  beforeEach(function() {
    filePath = undefined;
    delete process.env.MOCHA_FILE;
    delete process.env.PROPERTIES;
  });

  afterEach(function(done) {
    debug('after');
    if (stdout) {
      stdout.restore();
    }

    removeTestPath(done);
  });

  it('can produce an Obsidian JSON report', function(done) {
    var reporter = createReporter({mochaFile: 'test/mocha-output.json'});
    runTests(reporter, function() {
      verifyMochaSchema(reporter._runner, filePath);
      done();
    });
  });

});
