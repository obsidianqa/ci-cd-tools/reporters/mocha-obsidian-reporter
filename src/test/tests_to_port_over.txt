it('respects `process.env.MOCHA_FILE`', function(done) {
    process.env.MOCHA_FILE = 'test/output/results.xml';
    var reporter = createReporter();
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, process.env.MOCHA_FILE);
      done();
    });
  });

  it('respects `process.env.PROPERTIES`', function(done) {
    process.env.PROPERTIES = 'CUSTOM_PROPERTY:ABC~123';
    var reporter = createReporter({mochaFile: 'test/output/properties.xml'});
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, filePath, {
        properties: [
          {
            name: 'CUSTOM_PROPERTY',
            value: 'ABC~123',
          },
        ],
      });
      done();
    });
  });

  it('respects `--reporter-options mochaFile=`', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/results.json'});
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, filePath);
      done();
    });
  });

  it('respects `[hash]` pattern in test results report filename', function(done) {
    var dir = 'test/output/';
    var path = dir + 'results.[hash].json';
    var reporter = createReporter({mochaFile: path});
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, dir + getFileNameWithHash(dir));
      done();
    });
  });

  it('will create intermediate directories', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/foo/mocha.json'});
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, filePath);
      done();
    });
  });

  it('creates valid XML report for invalid message', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/mocha.json'});
    runTests(reporter, {invalidChar: '\u001b'}, function() {
    //   assertXmlEquals(reporter._xml, mockXml(reporter.runner.stats));
      done();
    });
  });

  it('creates valid XML report even if title contains ANSI character sequences', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/mocha.json'});
    runTests(reporter, {title: '[38;5;104m[1mFoo Bar'}, function() {
      verifyMochaFile(reporter.runner, filePath);
      done();
    });
  });

  it('outputs pending tests if "includePending" is specified', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/mocha.json', includePending: true});
    runTests(reporter, {includePending: true}, function() {
      verifyMochaFile(reporter.runner, filePath);
      done();
    });
  });

  it('can output to the console', function(done) {
    var reporter = createReporter({mochaFile: 'test/output/console.json', toConsole: true});

    var stdout = mockStdout();
    runTests(reporter, function() {
      verifyMochaFile(reporter.runner, filePath);

    //   var xml = stdout.output[0];
    //   assertXmlEquals(xml, mockXml(reporter.runner.stats));

      done();
    });
  });

  it('properly outputs tests when error in beforeAll', function(done) {
    var reporter = createReporter();
    var rootSuite = reporter.runner.suite;
    var suite1 = Suite.create(rootSuite, 'failing beforeAll');
    suite1.beforeAll('failing hook', function() {
      throw new Error('error in before');
    });
    suite1.addTest(createTest('test 1'));

    var suite2 = Suite.create(rootSuite, 'good suite');
    suite2.addTest(createTest('test 2'));

    runRunner(reporter.runner, function() {
      reporter.runner.dispose();
      expect(reporter._testsuites).to.have.lengthOf(3);
      expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal('failing beforeAll');
      expect(reporter._testsuites[1].testsuite[1].testcase).to.have.lengthOf(2);
      expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal('failing beforeAll "before all" hook: failing hook for "test 1"');
      expect(reporter._testsuites[1].testsuite[1].testcase[1].failure._attr.message).to.equal('error in before');
      expect(reporter._testsuites[2].testsuite[0]._attr.name).to.equal('good suite');
      expect(reporter._testsuites[2].testsuite[1].testcase).to.have.lengthOf(1);
      expect(reporter._testsuites[2].testsuite[1].testcase[0]._attr.name).to.equal('good suite test 2');
      done();
    });
  });

  describe('when "useFullSuiteTitle" option is specified', function() {
    it('generates full suite title', function(done) {
      var reporter = createReporter({useFullSuiteTitle: true });
      runTests(reporter, function() {
        expect(suiteName(reporter._testsuites[0])).to.equal('');
        expect(suiteName(reporter._testsuites[1])).to.equal('Root Suite Foo Bar');
        expect(suiteName(reporter._testsuites[2])).to.equal('Root Suite Another suite!');
        done();
      });
    });

    it('generates full suite title separated by "suiteTitleSeparatedBy" option', function(done) {
      var reporter = createReporter({useFullSuiteTitle: true, suiteTitleSeparatedBy: '.'});
      runTests(reporter, function() {
        expect(suiteName(reporter._testsuites[0])).to.equal('');
        expect(suiteName(reporter._testsuites[1])).to.equal('Root Suite.Foo Bar');
        expect(suiteName(reporter._testsuites[2])).to.equal('Root Suite.Another suite!');
        done();
      });
    });

    function suiteName(suite) {
      return suite.testsuite[0]._attr.name;
    }
  });

  describe('when "outputs" option is specified', function() {
    it('adds output/error lines to xml report', function(done) {
      var reporter = createReporter({outputs: true});

      var test = createTest('has outputs');
      test.consoleOutputs = [ 'hello', 'world' ];
      test.consoleErrors = [ 'typical diagnostic info', 'all is OK' ];

      var suite = Suite.create(reporter.runner.suite, 'with console output and error');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.length(3);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());
        expect(reporter._testsuites[1].testsuite[1].testcase[1]).to.have.property('system-out', 'hello\nworld');
        // expect(reporter._testsuites[1].testsuite[1].testcase[2]).to.have.property('system-err', 'typical diagnostic info\nall is OK');

        // expect(reporter._xml).to.include('<system-out>hello\nworld</system-out>');
        // expect(reporter._xml).to.include('<system-err>typical diagnostic info\nall is OK</system-err>');

        done();
      });
    });

    it('does not add system-out if no outputs/errors were passed', function(done) {
      var reporter = createReporter({outputs: true});
      var test = createTest('has outputs');
      var suite = Suite.create(reporter.runner.suite, 'with console output and error');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.length(1);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());

        // expect(reporter._xml).not.to.include('<system-out>');
        // expect(reporter._xml).not.to.include('<system-err>');

        done();
      });
    });

    it('does not add system-out if outputs/errors were empty', function(done) {
      var reporter = createReporter({outputs: true});
      var test = createTest('has outputs');
      test.consoleOutputs = [];
      test.consoleErrors = [];

      var suite = Suite.create(reporter.runner.suite, 'with console output and error');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.length(1);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());

        // expect(reporter._xml).not.to.include('<system-out>');
        // expect(reporter._xml).not.to.include('<system-err>');

        done();
      });
    });
  });

  describe('when "attachments" option is specified', function() {
    it('adds attachments to xml report', function(done) {
      var filePath = '/path/to/file';
      var reporter = createReporter({attachments: true});
      var test = createTest('has attachment');
      test.attachments = [filePath];

      var suite = Suite.create(reporter.runner.suite, 'with attachments');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.length(2);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());
        // expect(reporter._testsuites[1].testsuite[1].testcase[1]).to.have.property('system-out', '[[ATTACHMENT|' + filePath + ']]');

        // expect(reporter._xml).to.include('<system-out>[[ATTACHMENT|' + filePath + ']]</system-out>');

        done();
      });
    });

    it('does not add system-out if no attachments were passed', function(done) {
      var reporter = createReporter({attachments: true});
      var test = createTest('has attachment');

      var suite = Suite.create(reporter.runner.suite, 'with attachments');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.lengthOf(1);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());

        // expect(reporter._xml).to.not.include('<system-out>');

        done();
      });
    });

    it('does not add system-out if attachments array is empty', function(done) {
      var reporter = createReporter({attachments: true});
      var test = createTest('has attachment');
      test.attachments = [];

      var suite = Suite.create(reporter.runner.suite, 'with attachments');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.lengthOf(1);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());

        // expect(reporter._xml).to.not.include('<system-out>');

        done();
      });
    });

    it('includes both console outputs and attachments in XML', function(done) {
      var reporter = createReporter({attachments: true, outputs: true});
      var test = createTest('has attachment');
      var filePath = '/path/to/file';
      test.attachments = [filePath];
      test.consoleOutputs = [ 'first console line', 'second console line' ];

      var suite = Suite.create(reporter.runner.suite, 'with attachments and outputs');
      suite.addTest(test);

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites[1].testsuite[0]._attr.name).to.equal(suite.title);
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.length(2);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal(test.fullTitle());
        // expect(reporter._testsuites[1].testsuite[1].testcase[1]).to.have.property('system-out', 'first console line\nsecond console line\n[[ATTACHMENT|' + filePath + ']]');

        // expect(reporter._xml).to.include('<system-out>first console line\nsecond console line\n[[ATTACHMENT|' + filePath + ']]</system-out>');

        done();
      });
    });
  });

  describe('Output', function() {
    it('skips suites with empty title', function(done) {
      var reporter = createReporter();
      var suite = Suite.create(reporter.runner.suite, '');
      suite.root = false; // mocha treats suites with empty title as root, so not sure this is possible
      suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites).to.have.lengthOf(1);
        expect(reporter._testsuites[0].testsuite[0]._attr.name).to.equal('Root Suite');
        done();
      });
    });

    it('skips suites without testcases and suites', function(done) {
      var reporter = createReporter();
      Suite.create(reporter.runner.suite, 'empty suite');

      // mocha won't emit the `suite` event if a suite has no tests in it, so we won't even output the root suite.
      // See https://github.com/mochajs/mocha/blob/c0137eb698add08f29035467ea1dc230904f82ba/lib/runner.js#L723.
      runRunner(reporter.runner, function() {
        expect(reporter._testsuites).to.have.lengthOf(0);
        done();
      });
    });

    it('skips suites without testcases even if they have nested suites', function(done) {
      var reporter = createReporter();
      var suite1 = Suite.create(reporter.runner.suite, 'suite');
      Suite.create(suite1, 'nested suite');

      runRunner(reporter.runner, function() {
        // even though we have nested suites, there are no tests so mocha won't emit the `suite` event
        expect(reporter._testsuites).to.have.lengthOf(0);
        done();
      });
    });

    it('does not skip suites with nested tests', function(done) {
      var reporter = createReporter();
      var suite = Suite.create(reporter.runner.suite, 'nested suite');
      suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites).to.have.lengthOf(2);
        expect(reporter._testsuites[0].testsuite[0]._attr.name).to.equal('Root Suite');
        expect(reporter._testsuites[1].testsuite[1].testcase).to.have.lengthOf(1);
        expect(reporter._testsuites[1].testsuite[1].testcase[0]._attr.name).to.equal('nested suite test');
        done();
      });
    });

    it('does not skip root suite', function(done) {
      var reporter = createReporter();
      reporter.runner.suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites).to.have.lengthOf(1);
        expect(reporter._testsuites[0].testsuite[0]._attr.name).to.equal('Root Suite');
        expect(reporter._testsuites[0].testsuite[1].testcase).to.have.lengthOf(1);
        expect(reporter._testsuites[0].testsuite[1].testcase[0]._attr.name).to.equal('test');
        done();
      });
    });

    it('respects the `rootSuiteTitle`', function(done) {
      var name = 'The Root Suite!';
      var reporter = createReporter({rootSuiteTitle: name});
      reporter.runner.suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        expect(reporter._testsuites).to.have.lengthOf(1);
        expect(reporter._testsuites[0].testsuite[0]._attr.name).to.equal(name);
        done();
      });
    });

    it('uses "Mocha Tests" by default', function(done) {
      var reporter = createReporter();
      reporter.runner.suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        // expect(reporter._xml).to.include('testsuites name="Mocha Tests"');
        done();
      });
    });

    it('respects the `testsuitesTitle`', function(done) {
      var title = 'SuitesTitle';
      var reporter = createReporter({testsuitesTitle: title});
      reporter.runner.suite.addTest(createTest('test'));

      runRunner(reporter.runner, function() {
        // expect(reporter._xml).to.include('testsuites name="SuitesTitle"');
        done();
      });
    });
  });

  describe('Feature "Configurable classname/name switch"', function() {
    var mockedTestCase = {
      title: 'should behave like so',
      timestamp: 123,
      tests: '1',
      failures: '0',
      time: '0.004',
      fullTitle: function() {
        return 'Super Suite ' + this.title;
      },
    };

    it('should generate valid testCase for testCaseSwitchClassnameAndName default', function() {
      var reporter = createReporter();
      var testCase = reporter.getTestcaseData(mockedTestCase);
      expect(testCase.testcase[0]._attr.name).to.equal(mockedTestCase.fullTitle());
      expect(testCase.testcase[0]._attr.classname).to.equal(mockedTestCase.title);
    });

    it('should generate valid testCase for testCaseSwitchClassnameAndName=false', function() {
      var reporter = createReporter({testCaseSwitchClassnameAndName: false});
      var testCase = reporter.getTestcaseData(mockedTestCase);
      expect(testCase.testcase[0]._attr.name).to.equal(mockedTestCase.fullTitle());
      expect(testCase.testcase[0]._attr.classname).to.equal(mockedTestCase.title);
    });

    it('should generate valid testCase for testCaseSwitchClassnameAndName=true', function() {
      var reporter = createReporter({testCaseSwitchClassnameAndName: true});
      var testCase = reporter.getTestcaseData(mockedTestCase);
      expect(testCase.testcase[0]._attr.name).to.equal(mockedTestCase.title);
      expect(testCase.testcase[0]._attr.classname).to.equal(mockedTestCase.fullTitle());
    });
  });