
const fs = require('fs');
const path = require('path');
const debug = require('debug')('mocha-obsidian-reporter');
const mkdirp = require('mkdirp');
const md5 = require('md5');
const stripAnsi = require('strip-ansi');

const Mocha = require('mocha');
const {
  EVENT_RUN_BEGIN,
  EVENT_RUN_END,
  EVENT_TEST_FAIL,
  EVENT_TEST_PASS,
  EVENT_SUITE_BEGIN,
  EVENT_SUITE_END,
} = Mocha.Runner.constants;

const INVALID_CHARACTERS_REGEX =
// eslint-disable-next-line no-control-regex
/[\u0000-\u0008\u000B\u000C\u000E-\u001F\u007f-\u0084\u0086-\u009f\uD800-\uDFFF\uFDD0-\uFDFF\uFFFF\uC008]/g;

class ObsidianReporter {
  constructor(runner, options) {
    this._indents = 0;
    // const stats = runner.stats;

    this._options = this.configureDefaults(options);
    this._runner = runner;
    this._generateSuiteTitle = this._options.useFullSuiteTitle ? this.fullSuiteTitle : this.defaultSuiteTitle;
    this._Date = (options || {}).Date || Date;
    var testsuites = [];
    this._testsuites = testsuites;
    this._testsuitesJson = {
      executions: [],
    };
    this._passes = 0;
    this._failures = 0;


    runner
      .once(EVENT_RUN_BEGIN, () => {
        if (fs.existsSync(this._options.mochaFile)) {
          debug('removing report file', this._options.mochaFile);
          fs.unlinkSync(this._options.mochaFile);
        }
      })
      .on(EVENT_SUITE_BEGIN, (suite) => {
        this._testsuitesJson.startTimeUtc = new Date(new Date().toUTCString());
        // this.onSuiteBegin(suite);
      })
      .on(EVENT_SUITE_END, (suite) => {
        this._testsuitesJson.endTimeUtc = new Date(new Date().toUTCString());
        // this.onSuiteEnd(suite);
      })
      .on(EVENT_TEST_PASS, test => {
        const testCase = this.getTestcaseData(test);
        this._testsuitesJson.executions.push(testCase);
        this._passes++;
      })
      .on(EVENT_TEST_FAIL, (test, err) => {
        const testCase = this.getTestcaseData(test, err);
        this._testsuitesJson.executions.push(testCase);
        this._failures++;
      })
      .once(EVENT_RUN_END, () => {
        this.setSuiteSummaryData();
        this.flush(this._testsuitesJson);
      });
  }

  setSuiteSummaryData() {
    this._testsuitesJson.passes = this._passes;
    this._testsuitesJson.failures = this._failures;
    this._testsuitesJson.success = this._failures === 0;
  }

  findReporterOptions(options) {
    debug('Checking for options in', options);
    if (!options) {
      debug('No options provided');
      return {};
    }
    if (options.reporterOptions) {
      debug('Command-line options for mocha@6+');
      return options.reporterOptions;
    }

    debug('Looking for .mocharc.js options');
    return Object.keys(options).filter(function(key) { return key.indexOf('reporterOptions.') === 0; })
      .reduce(function(reporterOptions, key) {
        reporterOptions[key.substring('reporterOptions.'.length)] = options[key];
        return reporterOptions;
      }, {});
  }

  onSuiteBegin(suite) {
    if (!this.isInvalidSuite(suite)) {
      this._testsuitesJson.testsuites.push(this.getTestsuiteData(suite));
    }
  };
  onSuiteEnd(suite) {
    if (!this.isInvalidSuite(suite)) {
      var testsuite = this.lastSuite();
      if (testsuite) {
        var start = testsuite.timestamp;
        testsuite.time = this._Date.now() - start;
      }
    }
  };

  isInvalidSuite(suite) {
    return (!suite.root && suite.title === '') || (suite.tests.length === 0 && suite.suites.length === 0);
  }

  lastSuite() {
    return this._testsuitesJson.testsuites[this._testsuitesJson.testsuites.length - 1];
  }

  getTestsuiteData(suite) {
    var testSuite = {
      name: this._generateSuiteTitle(suite),
      timestamp: this._Date.now(),
      tests: suite.tests.length,
      testcases: [],
    };

    if (suite.file) {
      testSuite.file = suite.file;
    }
    var properties = this.generateProperties(this._options);
    if (properties.length) {
      testSuite.properties = properties;
    }
    return testSuite;
  }

  getTestcaseData(test, err) {
    var flipClassAndName = this._options.testCaseSwitchClassnameAndName;
    var name = stripAnsi(test.fullTitle());
    var classname = stripAnsi(test.title);
    var testcase = {
      testName: flipClassAndName ? classname : name,
      duration: (typeof test.duration === 'undefined') ? 0 : test.duration / 1000,
      classname: flipClassAndName ? name : classname,
      testFile: test.file,
      message: '',
      type: '',
      stackTrace: '',
    };

    var systemOutLines = [];
    if (this._options.outputs && (test.consoleOutputs && test.consoleOutputs.length > 0)) {
      systemOutLines = systemOutLines.concat(test.consoleOutputs);
    }
    if (this._options.attachments && test.attachments && test.attachments.length > 0) {
      systemOutLines = systemOutLines.concat(test.attachments.map(
        function(file) {
          return '[[ATTACHMENT|' + file + ']]';
        },
      ));
    }
    if (systemOutLines.length > 0) {
      testcase['system-out'] = this.removeInvalidCharacters(stripAnsi(systemOutLines.join('\n')));
    }

    if (this._options.outputs && (test.consoleErrors && test.consoleErrors.length > 0)) {
      testcase['system-err'] = this.removeInvalidCharacters(stripAnsi(test.consoleErrors.join('\n')));
    }

    if (err) {
      var message;
      if (err.message && typeof err.message.toString === 'function') {
        message = err.message + '';
      } else if (typeof err.inspect === 'function') {
        message = err.inspect() + '';
      } else {
        message = '';
      }
      var failureMessage = err.stack || message;

      testcase.message = this.removeInvalidCharacters(message) || '';
      testcase.type = err.name || '';
      testcase.stackTrace = this.removeInvalidCharacters(failureMessage);
      testcase.succeeded = false;
    } else {
      testcase.succeeded = true;
    }
    return testcase;
  };

  generateProperties(options) {
    var props = options.properties;
    if (!props) {
      return [];
    }
    return Object.keys(props).reduce(function(properties, name) {
      var value = props[name];
      properties.push({ property: { name: name, value: value } });
      return properties;
    }, []);
  }

  removeInvalidCharacters(input){
    if (!input) {
      return input;
    }
    return input.replace(INVALID_CHARACTERS_REGEX, '');
  };

  flush(testsuites){
    const jsonString = JSON.stringify(testsuites);
    this.writeJsonToDisk(jsonString, this._options.mochaFile);

    if (this._options.toConsole === true) {
      console.log(jsonString); // eslint-disable-line no-console
    }
  };

  writeJsonToDisk(json, filePath){
    if (filePath) {
      if (filePath.indexOf('[hash]') !== -1) {
        filePath = filePath.replace('[hash]', md5(json));
      }

      debug('writing file to', filePath);
      mkdirp.sync(path.dirname(filePath));

      try {
        fs.writeFileSync(filePath, json, 'utf-8');
      } catch (exc) {
        debug('problem writing results: ' + exc);
      }
      debug('results written successfully');
    }
  };

  configureDefaults(options) {
    var config = this.findReporterOptions(options);
    debug('options', config);
    config.mochaFile = this.getSetting(config.mochaFile, 'MOCHA_FILE', 'test-results.json');
    config.attachments = this.getSetting(config.attachments, 'ATTACHMENTS', false);
    config.properties = this.getSetting(config.properties, 'PROPERTIES', null, this.parsePropertiesFromEnv);
    config.toConsole = !!config.toConsole;
    // config.rootSuiteTitle = config.rootSuiteTitle || 'Root Suite';
    config.testsuitesTitle = config.testsuitesTitle || 'Mocha Tests';

    config.suiteTitleSeparatedBy = config.suiteTitleSeparatedBy || ' ';

    return config;
  }

  getSetting(value, key, defaultVal, transform) {
    if (process.env[key] !== undefined) {
      let envVal = process.env[key];
      return (typeof transform === 'function') ? transform(envVal) : envVal;
    }
    if (value !== undefined) {
      return value;
    }
    return defaultVal;
  }

  parsePropertiesFromEnv(envValue) {
    if (envValue) {
      debug('Parsing from env', envValue);
      return envValue.split(',').reduce(function(properties, prop) {
        var property = prop.split(':');
        properties[property[0]] = property[1];
        return properties;
      }, []);
    }
    return null;
  }

  defaultSuiteTitle(suite) {
    if (suite.root && suite.title === '') {
      return stripAnsi(this._options.rootSuiteTitle);
    }
    return stripAnsi(suite.title);
  }

  fullSuiteTitle(suite) {
    let parent = suite.parent;
    let title = [ suite.title ];

    while (parent) {
      if (parent.root && parent.title === '') {
        title.unshift(this._options.rootSuiteTitle);
      } else {
        title.unshift(parent.title);
      }
      parent = parent.parent;
    }

    return stripAnsi(title.join(this._options.suiteTitleSeparatedBy));
  }

}

module.exports = ObsidianReporter;
