# Obsidian Reporter for Mocha


Produces Obsidian JSON test results.

## Installation

```shell
$ npm install mocha-obsidian-reporter --save-dev
```

or as a global module
```shell
$ npm install -g mocha-obsidian-reporter
```

## Usage
Run mocha with `mocha-obsidian-reporter`:

```shell
$ mocha test --reporter mocha-obsidian-reporter
```
This will output a results file at `./test-results.json`.
You may optionally declare an alternate location for results XML file by setting
the environment variable `MOCHA_FILE` or specifying `mochaFile` in `reporterOptions`:

```shell
$ MOCHA_FILE=./path_to_your/file.json mocha test --reporter mocha-obsidian-reporter
```
or
```shell
$ mocha test --reporter mocha-obsidian-reporter --reporter-options mochaFile=./path_to_your/file.json
```
or
```javascript
var mocha = new Mocha({
    reporter: 'mocha-obsidian-reporter',
    reporterOptions: {
        mochaFile: './path_to_your/file.json'
    }
});
```
